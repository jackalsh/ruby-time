require 'socket' # Provides TCPServer and TCPSocket classes
require 'json'
require 'pry'

server_ip = '0.0.0.0'
server_port = 2346

# Initialize a TCPServer object that will listen
# on localhost:2345 for incoming connections.
server = TCPServer.new(server_ip, server_port)

puts "Timestamp server"
puts "Listening on #{server_ip}:#{server_port}"

# loop infinitely, processing one incoming
# connection at a time.
loop do

  # Wait until a client connects, then return a TCPSocket
  # that can be used in a similar fashion to other Ruby
  # I/O objects. (In fact, TCPSocket is a subclass of IO.)
  socket = server.accept

  sock_domain, remote_port, remote_hostname, remote_ip = socket.peeraddr
  puts "#{Time.now} : Request from #{remote_ip}:#{remote_port}"

  #binding.pry

  response = JSON.generate({
    host: Socket.gethostname,
    time: Time.now.to_i 
  })

  # We need to include the Content-Type and Content-Length headers
  # to let the client know the size and type of data
  # contained in the response. Note that HTTP is whitespace
  # sensitive, and expects each header line to end with CRLF (i.e. "\r\n")
  socket.print "HTTP/1.1 200 OK\r\n" +
               "Content-Type: text/plain\r\n" +
               "Content-Length: #{response.bytesize}\r\n" +
               "Connection: close\r\n"

  # Print a blank line to separate the header from the response body,
  # as required by the protocol.
  socket.print "\r\n"

  # Print the actual response body, which is just "Hello World!\n"
  socket.print response

  # Close the socket, terminating the connection
  socket.close
end


